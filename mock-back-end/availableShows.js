const config = {
  saleStartsDaysBefore: 25,
  showRunsForDays: 100,
  daysInBigHall: 60,
  ticketsPerDayBigHall: 10,
  bigHallCapacity: 200,
  smallHallCapacity: 100,
  ticketsPerDaySmallHall: 5,
};

const dateDaysDiff = (dateA, dateB) => {
  const dateAMili = new Date(dateA).getTime();
  const dateBMili = new Date(dateB).getTime();
  const diff = dateBMili - dateAMili;
  return diff / 1000 / 60 / 60 / 24;
};

const getShowStatus = (queryDate, showDate, openingDay) => {
  const daysUntilShowOpens = dateDaysDiff(queryDate, openingDay);
  if (daysUntilShowOpens > config.saleStartsDaysBefore) {
    return 'sale not started';
  } else if (daysUntilShowOpens < -config.showRunsForDays) {
    return 'in the past';
  } else {
    return 'open for sale';
  }
};

const getTicketsData = (queryDate, showDate, openingDay) => {
  const openShowDiff = dateDaysDiff(openingDay, showDate);
  const queryShowDiff = dateDaysDiff(queryDate, showDate);
  const isBigHall = openShowDiff <= config.daysInBigHall;
  const ticketsPerDay = isBigHall
    ? config.ticketsPerDayBigHall
    : config.ticketsPerDaySmallHall;
  const capacity = isBigHall
    ? config.bigHallCapacity
    : config.smallHallCapacity;
  if (queryShowDiff > config.saleStartsDaysBefore) {
    return { ticketsLeft: capacity, ticketsAvailable: 0 };
  } else {
    return {
      ticketsLeft:
        capacity -
        (config.saleStartsDaysBefore - queryShowDiff - 1) * ticketsPerDay,
      ticketsAvailable: ticketsPerDay,
    };
  }
};

const availableShows = db => (queryDate, showDate) => ({
  inventory: db
    .filter(show => show.openingDay <= showDate)
    .reduce((acc, curr) => {
      const status = getShowStatus(queryDate, showDate, curr.openingDay);
      const { ticketsLeft, ticketsAvailable } = getTicketsData(
        queryDate,
        showDate,
        curr.openingDay
      );
      const showData = {
        title: curr.title,
        tickets_left: ticketsLeft,
        tickets_available: ticketsAvailable,
        status,
      };

      return acc.some(
        entry => entry && entry.genre && entry.genre === curr.genre
      )
        ? acc.map(
            showType =>
              showType.genre === curr.genre
                ? {
                    ...showType,
                    shows: [...showType.shows, showData],
                  }
                : showType
          )
        : [
            ...acc,
            {
              genre: curr.genre,
              shows: [showData],
            },
          ];
    }, []),
});

module.exports = {
  dateDaysDiff,
  availableShows,
  getShowStatus,
  getTicketsData,
};
