const express = require('express');
const db = require('./db.json');

const app = express();
const port = process.env.PORT || 5000;

const availableShows = require('./availableShows').availableShows(db);
const isValidDateFormat = require('../src/isValidDateFormat');

app.get('/api/tickets/', (req, res) => {
  const {
    'query-date': queryDate = null,
    'show-date': showDate = null,
  } = req.query;

  if (isValidDateFormat(queryDate) && isValidDateFormat(showDate)) {
    res.send(availableShows(queryDate, showDate));
  } else {
    res.status(400).send('Please input dates in the format YYYY-MM-DD');
  }
});

app.listen(port, () => console.log(`Listening on port ${port}`));
