const {
  dateDaysDiff,
  availableShows,
  getShowStatus,
  getTicketsData,
} = require('../../mock-back-end/availableShows');

const testDb = [
  {
    id: 0,
    title: 'cats',
    openingDay: '2017-06-01',
    genre: 'musical',
  },
  {
    id: 1,
    title: 'comedy of errors',
    openingDay: '2017-07-01',
    genre: 'comedy',
  },
  {
    id: 2,
    title: 'everyman',
    openingDay: '2017-08-01',
    genre: 'drama',
  },
];

const testAvailableShows = availableShows(testDb);

test('dateDaysDiff returns days of difference between two string dates', () => {
  expect(dateDaysDiff('2017-01-01', '2017-07-01')).toBe(181);
  expect(dateDaysDiff('2018-01-01', '2017-07-01')).toBe(-184);
  expect(dateDaysDiff('2018-07-07', '2018-07-06')).toBe(-1);
  expect(dateDaysDiff('2018-07-07', '2018-07-07')).toBe(0);
  expect(dateDaysDiff('2018-07-07', '2018-07-08')).toBe(1);
  expect(dateDaysDiff('not a date', '2018-07-08')).toBeNaN;
});

test('getShowStatus returns proper show status', () => {
  expect(getShowStatus('2017-01-01', '2017-07-01', '2017-06-01')).toBe(
    'sale not started'
  );
  expect(getShowStatus('2017-01-01', '2017-07-01', '2017-07-01')).toBe(
    'sale not started'
  );

  expect(getShowStatus('2017-08-01', '2017-08-15', '2017-06-01')).toBe(
    'open for sale'
  );
  expect(getShowStatus('2017-08-01', '2017-08-15', '2017-07-01')).toBe(
    'open for sale'
  );
  expect(getShowStatus('2017-08-01', '2017-08-15', '2017-08-01')).toBe(
    'open for sale'
  );
  expect(getShowStatus('2018-08-01', '2018-08-15', '2017-08-01')).toBe(
    'in the past'
  );
});

test('getTicketsData returns amount of tickets left and to be sold that day', () => {
  expect(getTicketsData('2017-01-01', '2017-07-01', '2017-06-01')).toEqual({
    ticketsLeft: 200,
    ticketsAvailable: 0,
  });
  expect(getTicketsData('2017-01-01', '2017-07-01', '2017-07-01')).toEqual({
    ticketsLeft: 200,
    ticketsAvailable: 0,
  });

  expect(getTicketsData('2017-08-01', '2017-08-15', '2017-06-01')).toEqual({
    ticketsLeft: 50,
    ticketsAvailable: 5,
  });
  expect(getTicketsData('2017-08-01', '2017-08-15', '2017-07-01')).toEqual({
    ticketsLeft: 100,
    ticketsAvailable: 10,
  });
  expect(getTicketsData('2017-08-01', '2017-08-15', '2017-08-01')).toEqual({
    ticketsLeft: 100,
    ticketsAvailable: 10,
  });
});

test('testAvailableShows returns expected JSON format and values', () => {
  expect(testAvailableShows('2017-01-01', '2017-07-01')).toEqual({
    inventory: [
      {
        genre: 'musical',
        shows: [
          {
            title: 'cats',
            tickets_left: 200,
            tickets_available: 0,
            status: 'sale not started',
          },
        ],
      },
      {
        genre: 'comedy',
        shows: [
          {
            title: 'comedy of errors',
            tickets_left: 200,
            tickets_available: 0,
            status: 'sale not started',
          },
        ],
      },
    ],
  });
  expect(testAvailableShows('2017-08-01', '2017-08-15')).toEqual({
    inventory: [
      {
        genre: 'musical',
        shows: [
          {
            title: 'cats',
            tickets_left: 50,
            tickets_available: 5,
            status: 'open for sale',
          },
        ],
      },
      {
        genre: 'comedy',
        shows: [
          {
            title: 'comedy of errors',
            tickets_left: 100,
            tickets_available: 10,
            status: 'open for sale',
          },
        ],
      },
      {
        genre: 'drama',
        shows: [
          {
            title: 'everyman',
            tickets_left: 100,
            tickets_available: 10,
            status: 'open for sale',
          },
        ],
      },
    ],
  });
});
