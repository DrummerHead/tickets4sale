const isValidDateFormat = require('../isValidDateFormat');

test('isValidDateFormat returns true for well formatted date strings', () => {
  expect(isValidDateFormat('2017-01-01')).toBe(true);
  expect(isValidDateFormat('2017-08-01')).toBe(true);
  expect(isValidDateFormat('9999-99-99')).toBe(true);
});

test('isValidDateFormat returns false for badly formatted date strings', () => {
  expect(isValidDateFormat('2017/01/01')).toBe(false);
  expect(isValidDateFormat('17-08-01')).toBe(false);
  expect(isValidDateFormat('08-01-2018')).toBe(false);
  expect(isValidDateFormat('inextricable')).toBe(false);
  expect(isValidDateFormat(42)).toBe(false);
  expect(isValidDateFormat(true)).toBe(false);
});
