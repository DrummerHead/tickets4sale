import React from 'react';
import axios from 'axios';
import styled, { injectGlobal } from 'styled-components';
import InventoryTable from './InventoryTable';
import priceData from './priceData';
import isValidDateFormat from './isValidDateFormat';

injectGlobal`
  * {
    box-sizing: border-box;
  }
  body {
    font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
  }
`;

const Title = styled.h1`
  font-weight: 900;
  text-align: center;
  font-size: 4em;
  padding: 0.4rem 0 0.1rem;
  color: #000c;
`;

const Form = styled.form`
  display: flex;
  flex-flow: row wrap;
  padding: 0.5em 0.2em;
  border-style: solid none;
  border-width: 0.2em;
  border-color: #000c;
`;

const FormField = styled.label`
  display: flex;
  flex-flow: row nowrap;
  flex: 1 0;
  align-items: center;

  & span {
    padding: 0 0.5em 0 0;
  }

  & input {
    flex: 1 0;
    align-self: stretch;
    padding: 0 0.6rem;
    font-size: 1em;
  }
`;

const Button = styled.button`
  display: inline-block;
  vertical-align: middle;
  padding: 0.85em 1em;
  border: 1px solid transparent;
  border-radius: 0;
  transition: background-color 0.25s ease-out, color 0.25s ease-out;
  font-family: inherit;
  font-size: 0.9rem;
  appearance: none;
  line-height: 1;
  text-align: center;
  background-color: #1779ba;
  color: #fefefe;
  cursor: pointer;

  &:hover,
  &:focus {
    background-color: #14679e;
    color: #fefefe;
  }
`;

const Error = styled.span`
  color: red;
  flex: 1 0 100%;
  padding: 0.6em 0 0.1em;
  text-align: center;
`;

class App extends React.Component {
  state = {
    inventory: [],
    hasFetched: false,
    hasError: false,
    showDate: '2018-09-19',
    queryDate: new Date().toISOString().replace(/T.*$/, ''),
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    axios
      .get(
        `/api/tickets?query-date=${this.state.queryDate}&show-date=${
          this.state.showDate
        }`
      )
      .then(res =>
        this.setState({ inventory: res.data.inventory, hasFetched: true })
      )
      .catch(err => console.error(err));
  };

  handleDateChange = ev => this.setState({ showDate: ev.target.value });

  handleDateInput = ev => {
    ev.preventDefault();

    if (isValidDateFormat(this.state.showDate)) {
      this.fetchData();
    } else {
      this.setState({ hasError: true });
    }
  };

  render() {
    return (
      <div>
        <Title>Tickets4Sale</Title>
        <Form onSubmit={this.handleDateInput}>
          <FormField>
            <span>Show date: </span>
            <input
              type="date"
              onChange={this.handleDateChange}
              value={this.state.showDate}
            />
          </FormField>
          <Button type="submit">Search</Button>
          {this.state.hasError && (
            <Error>Please input a date in the format YYYY-MM-DD</Error>
          )}
        </Form>
        {this.state.hasFetched ? (
          this.state.inventory.length > 0 ? (
            this.state.inventory.map(genrePack => (
              <InventoryTable
                key={genrePack.genre}
                inventory={genrePack}
                priceData={priceData}
              />
            ))
          ) : (
            <span>No shows to show for that date</span>
          )
        ) : (
          <span>Loading...</span>
        )}
      </div>
    );
  }
}

export default App;
