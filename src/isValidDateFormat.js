const isValidDateFormat = dateString =>
  typeof dateString === 'string' && /^\d{4}-\d\d-\d\d$/.test(dateString);

module.exports = isValidDateFormat;
