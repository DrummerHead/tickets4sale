import React from 'react';
import styled from 'styled-components';

const Table = styled.table`
width: 100%
text-align: left;
border-collapse: collapse;
border-style: solid none;
border-width: .5em;
border-color: #000c;

& td,
& th {
  padding: .5em .75em;
}

& td:first-child,
& th:first-child {
  min-width: 22em;
  text-transform: capitalize;
}

& tr:nth-child(2n) {
  background-color: #c0cedd80;
}
`;

const Title = styled.h2`
font-weight: 400;
font-size: 2.2em;
margin: 2.5rem 0 1rem 0.7rem;

& span {
  font-weight: 200;
}
`;

const InventoryTable = props => (
  <div>
    <Title>
      {props.inventory.genre} &#8212;{' '}
      <span>price: {props.priceData[props.inventory.genre]}</span>
    </Title>
    <Table>
      <thead>
        <tr>
          <th>Title</th>
          <th>Tickets left</th>
          <th>Tickets available</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {props.inventory.shows.map(show => (
          <tr key={show.title}>
            <td>{show.title.toLowerCase()}</td>
            <td>{show.tickets_left}</td>
            <td>{show.tickets_available}</td>
            <td>{show.status}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  </div>
);

export default InventoryTable;
