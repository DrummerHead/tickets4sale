# Tickets4Sale

![Tickets4Sale Screnshot](https://i.imgur.com/vhsyHAy.png)

This is a test project for a hypothetical ticket selling app. It consists of a backend API and a light front end.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Installation

After pulling the project, run

```
yarn install
```

## Running

Run the mock back end with:

```
yarn mbe
```

Check `http://localhost:5000/api/tickets?query-date=2017-10-14&show-date=2017-10-17` for an example result

All code associated to the back-end is at `./mock-back-end/` with the exception of date string validation (shared with the FE) which is at `./src/isValidDateFormat.js`


After the BE is running, front end with:

```
yarn start
```

Check `http://localhost:3000/` to see the front end. Any request the front-end can't resolve gets proxied to `http://localhost:5000/` and that's how the FE can talk to the BE in another port.

## Tests

Run the tests with

```
yarn test
```
